package SignUp;

import Base.BaseClass;
import Pages.CreateAccountPage;
import Pages.SignInPage;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import static utility.ReadJSONFile.addToArray;


public class SignUpTest extends BaseClass {

        List<String> data;
        String email;
        String password;
        CreateAccountPage createAccountPage;
        SignInPage signInPage;

    @Test()
    public void testSignUp() throws IOException {
        email =getRandomEmail();
        password= getRandomPassword();
        addToArray(email, password);
        getData(password);
        homePage.goToSignIn();

        signInPage = new SignInPage(driver);
        signInPage.insertEmailSignIn(email);

        createAccountPage = new CreateAccountPage(driver);
        createAccountPage.populateData(data);
        Assert.assertEquals(createAccountPage.getLogInStatus(), "Sign out", "You are not logged in");
    }
    private String getRandomEmail(){
        return RandomStringUtils.randomAlphanumeric(20) + "@gmail.com";
    }
    private String getRandomPassword(){
        return RandomStringUtils.randomAlphanumeric(10);
    }
    private void getData (String pass) {
       data = new LinkedList<String> (Arrays.asList("male", "Hasan", "Okanović", "3", "September", "1995",
                "Hasan", "Okanović", "Hansstrasse 6, 43214 Cologne", "New York", "Florida",
                "44242", " +42424252", "thisissecond@gmail.com"));
       data.add(3, pass);
    }
}

    /*  0- gender
        1- first name
        2- second name
        3- password
        4- day birth
        5- month birth
        6- year birth
        7- first name address
        8- second name address
        9- address
        10- city
        11- state
        12- zip
        13- phone
        14- alias address */
