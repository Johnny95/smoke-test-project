package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SignInPage {

    private WebDriver driver;
    private final By emailSignUpFieldID = By.id("email_create");
    private final By createAccButtonID = By.id("SubmitCreate");
    private final By emailLogInFieldID = By.id("email");
    private final By passwordFieldID = By.id("passwd");
    private final By signInButtonID = By.id("SubmitLogin");

    public SignInPage(WebDriver driver){
        this.driver = driver;
    }

    public CreateAccountPage insertEmailSignIn(String emailSingUp){
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(emailSignUpFieldID));
        driver.findElement(emailSignUpFieldID).sendKeys(emailSingUp);
        driver.findElement(createAccButtonID).click();
        return new CreateAccountPage (driver);
    }
    public MyAccountPage signIn(String emailLogIn, String passLogIn){
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(signInButtonID));
        WebElement signInButton = driver.findElement(signInButtonID);
        if(signInButton.isDisplayed()){
            driver.findElement(emailLogInFieldID).sendKeys(emailLogIn);
            driver.findElement(passwordFieldID).sendKeys(passLogIn);
            signInButton.click();
            return new MyAccountPage(driver);
        }
        return null;
    }
}
