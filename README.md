I used the Selenium framework with Java to automate the smoke test. The tests can be either run together or individually. If they are run together, please make sure that the runner is set to 'All' TestNG.

The tests are written according to the POM model. Therefore, page code and test code are separated. 

Description of the tests:
- Checkout test - verifies wheteher a user is able to make a purchase. I have also made several verifications along the way.
- LogInTest - verifies whether user can log in and log out of the application
- Search - After searching 'Printed Dresses', it saves the results in .txt file and verifies wheter all categories are present
- Signup - creates a new account using random email and password

In case you have any questions regarding the tests, please feel free to reach me out: okanovichasan@outlook.com
